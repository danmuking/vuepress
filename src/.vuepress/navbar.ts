import { navbar } from "vuepress-theme-hope";

export default navbar([
  "/",
  {
    text: "Java",
    link: "/posts/docs/Java/JAVA学习路书-2024年最新版.md",
  },
  {
    text: "学习之路",
    link: "/posts/docs/学习之路/创作经历/1.搭建全自动博客工作流.md",
  },
]);
