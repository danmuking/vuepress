import { defineUserConfig } from "vuepress";
import theme from "./theme.js";


export default defineUserConfig({
  base: "/",

  lang: "zh-CN",
  // 左上角标题
  title: "单木在Coding",
  description: "希望能帮到你！",
  markdown: {
    headers: {
      level: [1,2]
    }
  },
  theme,
  // 和 PWA 一起启用
  // shouldPrefetch: false,
});



